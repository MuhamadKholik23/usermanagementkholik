package models

type User struct {
	Id       int    `json:"id" gorm:"primary_key"`
	Username string `json:"username" gorm:"unique;not null;type:varchar(100)"`
	Password string `json:"password" gorm:"type:varchar(100)"`
	Name     string `json:"name" gorm:"type:varchar(100)"`
}

type Pagination struct {
	Limit int `json:"limit"`
	Page  int `json:"page"`
}
