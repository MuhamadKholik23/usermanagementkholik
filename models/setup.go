package models

import (
	"database/sql"

	_ "github.com/lib/pq"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// connect + migrate ke db
func ConnectDB() *gorm.DB {
	dsn := "host=localhost user=postgres password=070200 dbname=UserManagement port=5432 sslmode=disable TimeZone=Asia/Jakarta"
	db, err := gorm.Open(postgres.Open(dsn))

	if err != nil {
		panic("Gagal connect ke db")
	}
	db.AutoMigrate(&User{})
	return db
}

// connect db type *sql
func ConnectTypeSql() (*sql.DB, error) {
	dsn := "host=localhost user=postgres password=070200 dbname=UserManagement port=5432 sslmode=disable TimeZone=Asia/Jakarta"
	db, err := sql.Open("postgres", dsn)

	if err != nil {
		return nil, err
	}

	return db, nil
}

// get user page
func GetAllUsersPagination(user *User, pagination *Pagination) (*[]User, error) {
	db := ConnectDB()
	var users []User
	offset := (pagination.Page - 1) * pagination.Limit
	queryBuider := db.Limit(pagination.Limit).Offset(offset)
	result := queryBuider.Model(&User{}).Where(user).Find(&users)
	if result.Error != nil {
		msg := result.Error
		return nil, msg
	}
	return &users, nil
}

// hash pw
func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
