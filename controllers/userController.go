package controllers

import (
	"UserManagement/models"
	"UserManagement/request"
	"UserManagement/utils"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

//Menampilkan semua user
func GetAllUser(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	var user []models.User
	db.Find(&user)
	c.JSON(http.StatusOK, gin.H{"data": user, "Jumlah": len(user)})
}

// Insert User
func InsertUser(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	//validasi input/masukkan
	var bodyUser request.UserRequest
	err := c.ShouldBindJSON(&bodyUser)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//proses input
	user := models.User{
		Username: bodyUser.Username,
		Password: bodyUser.Password,
		Name:     bodyUser.Name,
	}
	user.Password, _ = models.HashPassword(user.Password)

	db.Create(&user)

	c.JSON(http.StatusOK, gin.H{"data": user})
}

// find by id
func FindById(c *gin.Context) {
	db, err := models.ConnectTypeSql()
	var (
		user   models.User
		result gin.H
	)
	id := c.Param("id")
	row := db.QueryRow("select id, username, name from users where id = $1", id)
	err = row.Scan(&user.Id, &user.Name, &user.Username)
	if err != nil {
		result = gin.H{"Hasil": "Tidak ada data yang ditemukan"}
	} else {
		result = gin.H{"Hasil": user, "Jumlah": "1"}
	}
	c.JSON(http.StatusOK, result)

}

// Ubah data mahasiswa
func UpdateUser(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	//cek data
	var user models.User
	err := db.Where("id = ?", c.Param("id")).First(&user).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Data user tidak ditemukan"})
		return
	}

	//validasi input/masukkan
	var bodyUser models.User
	errr := c.ShouldBindJSON(&bodyUser)
	if errr != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	bodyUser.Password, _ = models.HashPassword(bodyUser.Password)
	//proses ubah data
	db.Model(&user).Updates(bodyUser)

	c.JSON(http.StatusOK, gin.H{"data": user})
}

// Hapus data user
func DeleteUser(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	//cek data
	var user models.User
	err := db.Where("id = ?", c.Param("id")).First(&user).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Data user tidak ditemukan"})
		return
	}

	//hapus data
	db.Delete(&user)

	c.JSON(http.StatusOK, gin.H{"data": user, "status": "Sudah dihapus"})
}

//Get user page
func GetAllUsersByPage(c *gin.Context) {
	pagination := utils.GeneratePaginationFromRequest(c)
	var user models.User
	userLists, err := models.GetAllUsersPagination(&user, &pagination)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return

	}
	c.JSON(http.StatusOK, gin.H{
		"data": userLists,
	})

}
