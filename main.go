package main

import (
	"UserManagement/controllers"
	"UserManagement/models"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	// auto migrate
	db := models.ConnectDB()
	r.Use(func(c *gin.Context) {
		c.Set("db", db)
		c.Next()
	})

	r.GET("/GetAllUser", controllers.GetAllUser)
	r.POST("/InsertUser", controllers.InsertUser)
	r.GET("/GetById/:id", controllers.FindById)
	r.PUT("/UpdateUser/:id", controllers.UpdateUser)
	r.DELETE("/DeleteUser/:id", controllers.DeleteUser)
	r.GET("/GetByPage", controllers.GetAllUsersByPage)
	r.Run(":8080")
}
