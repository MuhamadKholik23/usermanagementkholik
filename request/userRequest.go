package request

type UserRequest struct {
	Username string `json:"username" gorm:"unique;not null;type:varchar(100)" binding:"required,min=3"`
	Password string `json:"password" gorm:"type:varchar(100)" binding:"required,min=7"`
	Name     string `json:"name" gorm:"type:varchar(100)" binding:"required,min=3"`
}
